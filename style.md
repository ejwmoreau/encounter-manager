# Markdown Pages vs Cards

* The encounter notes can be added all in a single page, or across multiple pages
* Each title + following content will be displayed as a card
* The column which the card will be displayed in depends on the heading level given
    * Heading 1 will be in the first column, Heading 2 in the second, and Heading 3 in the third

# Titles/Headings

```
# This title/card will be displayed in the first column
```

# Styled Text

```
Just type as normal, in _italics_, in __bold__, or even use:
* bullet points

and

1. numbered points
```

# Links

* Links will need to include the path from the source directory to the file being referenced
* Referencing a specific title will then need to have a '#' and the full title

```
This is a link to another card [dungeons/cragmaw-castle.md#Castle Entrance]
```
